// License: MIT (c) GitLab Inc.
package cors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PermissiveCORSInjection {
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String paramValue = request.getParameter("tainted");

        if (paramValue != null) {
          // ok:java_cors_rule-PermissiveCORSInjection
          response.setHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", paramValue);

          // ruleid:java_cors_rule-PermissiveCORSInjection
          response.setHeader("Access-Control-Allow-Origin", paramValue);

          // ruleid:java_cors_rule-PermissiveCORSInjection
          response.addHeader("Access-Control-Allow-Origin", paramValue);

          // ruleid:java_cors_rule-PermissiveCORSInjection
          response.addHeader("access-control-allow-origin", paramValue);

          String headerName = "ACCESS-CONTROL-ALLOW-ORIGIN";

          // ruleid:java_cors_rule-PermissiveCORSInjection
          response.addHeader(headerName, paramValue);

          return;
        }

        // ok:java_cors_rule-PermissiveCORSInjection
        response.setHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "*");
    }
}

